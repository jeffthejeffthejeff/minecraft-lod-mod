package com.seibel.lod.fabric.wrappers.modAccessor;

import com.seibel.lod.core.wrapperInterfaces.modAccessor.IStarlightAccessor;


public class StarlightAccessor implements IStarlightAccessor {

	@Override
	public String getModName() {
		return "Starlight-Fabric-1.18.X";
	}
	
	public StarlightAccessor() {
		
	}
}
